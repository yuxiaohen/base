#!/usr/bin/env
#-*- encoding:utf-8 -*-
from PyQt5.QtWidgets import *

import rs.sysfix.cn.fdb.logic.qt.ui.plugins.drag_target_container.ui_drag_target_container as m_ui


class DragTargetContainer(QWidget):
    def __init__(self,parent = None):
        super(DragTargetContainer,self).__init__(parent)

        self.ui = m_ui.Ui_DragTargetContainer()
        self.ui.setupUi(self)

        self.target = parent
        self.double_clk_to_max = True
        self.pressing = False
        self.setMouseTracking(True)

    def set_target(self,target):
        if isinstance(target,QWidget) is False:
            raise ValueError
        self.target = target

    def set_double_clk_to_max(self,max):
        self.double_clk_to_max = max

    def mouseDoubleClickEvent(self, e):
        if self.target is None:
            return
        if self.double_clk_to_max is False:
            return
        if self.target.isMaximized():
            self.target.showNormal()
        else:
            self.target.showMaximized()

    def mousePressEvent(self, e):
        self.pressing = True
        self.snap_pos = e.pos()

    def mouseReleaseEvent(self, e):
        self.pressing = False

    def mouseMoveEvent(self, e):
        if self.target is None or self.pressing is False:
            e.accept()
            return
        if self.target.isMaximized():
            self.target.showNormal()
            #还要等比缩放光标的位置
            e.accept()
            return
        now_pos = e.pos()
        horizontal_delta = now_pos.x() - self.snap_pos.x()
        vertical_delta = now_pos.y() - self.snap_pos.y()
        self.target.move(self.target.x()+horizontal_delta,self.target.y()+vertical_delta)

if __name__ == '__main__':
    import sys
    app = QApplication(sys.argv)

    widget = DragTargetContainer()
    widget.show()

    app.exec()