# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'drag_target_container.ui'
#
# Created: Tue May 20 07:48:25 2014
#      by: PyQt5 UI code generator 5.2.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_DragTargetContainer(object):
    def setupUi(self, DragTargetContainer):
        DragTargetContainer.setObjectName("drag_target_container")
        DragTargetContainer.resize(370, 76)

        self.retranslateUi(DragTargetContainer)
        QtCore.QMetaObject.connectSlotsByName(DragTargetContainer)

    def retranslateUi(self, DragTargetContainer):
        _translate = QtCore.QCoreApplication.translate
        DragTargetContainer.setWindowTitle(_translate("drag_target_container", "Form"))

