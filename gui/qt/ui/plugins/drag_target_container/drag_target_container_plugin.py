#!/usr/bin/env
#-*- encoding:utf-8 -*-
from PyQt5.QtDesigner import *

import rs.sysfix.cn.fdb.logic.qt.ui.plugins.drag_target_container.drag_target_container as m_entity


class DragTargetContainerPlugin(QPyDesignerCustomWidgetPlugin):
    def __init__(self, parent = None):
        super(DragTargetContainerPlugin,self).__init__(parent)

        self.initialized = False

    def initialize(self, form_editor):
        if self.initialized:
            return
        self.initialized = True

    def isInitialized(self):
        return self.initialized

    def createWidget(self, parent):
        return m_entity.DragTargetContainer(parent)

    def name(self):
        return 'drag_target_container'

    def group(self):
        return 'FDB Qt Ui Plugins'

    def isContainer(self):
        return True

    def icon(self):
        return None

    def toolTip(self):
        return 'Drag Target Container'

    def whatsThis(self):
        return None

    def domXml(self):
        return '<widget class="DragTargetContainer" name="dragtargetcontainer">\n' \
               ' <property name="toolTip" >\n' \
               '  <string></string>\n' \
               ' </property>\n' \
               ' <property name="whatsThis" >\n' \
               '  <string>EliteAndroid8000</string>\n' \
               ' </property>\n' \
               '</widget>\n'

    def includeFile(self):
        return 'rs.sysfix.cn.fdb.logic.qt.ui.plugins.drag_target_container.drag_target_container'


if __name__ == '__main__':
    import sys
    from PyQt5.QtWidgets import *
    app = QApplication(sys.argv)

    plugin = DragTargetContainerPlugin()
    plugin.initialize(None)

    app.exec()